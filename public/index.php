<?php

$request = $_SERVER['REQUEST_URI'];

switch ($request) {
    case '/':
    case '' :
        require __DIR__ . '/../src/view/Product_List_View.php';
        break;
    case '/addproduct' :
        require __DIR__ . '/../src/view/Product_Add_View.php';
        break;
    case '/dvd' :
        require __DIR__ . '/../src/view/Dvd_Add_View.php';
        break;
    case '/book' :
        require __DIR__ . '/../src/view/Book_Add_View.php';
        break;
    case '/furniture' :
        require __DIR__ . '/../src/view/Furniture_Add_View.php';
        break;
    default:
        http_response_code(404);
        require __DIR__ . '/../src/view/404_View.php';
        break;
}