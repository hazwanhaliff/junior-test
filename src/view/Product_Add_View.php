<?php

require '../vendor/autoload.php';

use app\controller\Product_Controller;
use app\model\Dvd;
use app\model\Book;
use app\model\Furniture;

if (isset($_POST["submit"])) {

    $register = new Product_Controller;
    $register->checkSkuTaken();
    if($register->checkSkuTaken() == true)
    {
        $name_error = "Sorry... sku already taken";
        $sku = $_POST['sku'];
        $name = $_POST['name'];
        $price = $_POST['price'];
        

    } else {

        $dvd = new Dvd();
        $book = new Book();
        $furniture = new Furniture();
        $res = array($dvd->setProduct(), $book->setProduct(), $furniture->setProduct());

        return header("Location:/");

    }
    
} 

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="/css/style.css" rel="stylesheet">

    <title>Product Add</title>
</head>

<body>

    <header>
        <h2 style="display: inline; " class="header">Add Product</h2>
        <div style="display: inline; width:100%;" class="buttons">
            <div style="float: right;" class="action_btn">
                <button name="submit" form="product_form" type="submit">Save</button>
                <a href="/"><input type="reset" value="Cancel"></input></a>
            </div>
        </div>

    </header>
    <hr>

    <div class="content">
        <div class="container">
            <form action="/addproduct" id="product_form" method="post">
                <div class="row">
                    <div class="col-25">
                        <label for="SKU">SKU</label>
                    </div>
                    <div class="col-45">
                        <input type="text" id="sku" name="sku" placeholder="#SKU" value="<?php if (isset($sku)){
                            echo $sku; }; ?>" required>
                        <?php if (isset($name_error)) : ?>
                            <span><?php echo $name_error; ?></span>
                        <?php endif ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-25">
                        <label for="name">Name</label>
                    </div>
                    <div class="col-45">
                        <input type="text" id="name" name="name" placeholder="#Name" value="<?php if (isset($name)){
                            echo $name; }; ?>" required>
                    </div>
                </div>
                <div class="row">
                    <div class="col-25">
                        <label for="country">Price($)</label>
                    </div>
                    <div class="col-45">
                        <input type="text" id="price" name="price" pattern="[0-9]+(\.[0-9]{1,2})?" value="<?php if (isset($price)){
                            echo $price; }; ?>" title="Please, provide data of indicated type" placeholder="#Price" value="<?php echo $price;?>" required>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-25">
                        <label for="country">Type Switcher</label>
                    </div>
                    <div class="col-45">
                        <select id="productType" name="productType" onchange="aload(this.value);" required>
                            <option value="" disabled selected>Type Switcher</option>
                            <option id="DVD" value="dvd">DVD</option>
                            <option id="Book" value="book">Book</option>
                            <option id="Furniture" value="furniture">Furniture</option>
                        </select>
                    </div>
                </div>
                <br>
                <div class="content">
                    <div id="container">
                        <script>
                            function aload(type) {
                                fetch(type)
                                    .then(res => res.text())
                                    .then((txt) => {
                                        document.getElementById("container").innerHTML = txt;
                                    })
                            }
                        </script>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <hr>
    <footer>
        <div class="footer">
            <p>Scandiweb Test assignment</p>
        </div>
    </footer>

</body>

</html>