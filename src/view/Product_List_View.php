<?php

require '../vendor/autoload.php';

use app\model\Dvd;
use app\model\Book;
use app\model\Furniture;

use app\model\Product;

if (isset($_POST["delete-product-btn"])) {

    $register = new Product;
    $register->massDelete();

    return header("Location:/");
} 

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="/css/style.css" rel="stylesheet">

    <title>Product List</title>
</head>

<body>

    <div class="content">
        <div class="container">
            <header>
                <h2 style="display: inline; " class="header">Product List</h2>
                <div style="display: inline; width:100%;" class="buttons">
                    <div style="float: right;" class="action_btn">
                        <a href="/addproduct"><button>ADD</button></a>
                        <button name="delete-product-btn" id="delete-product-btn" form="productlist_form" type="submit" value="Delete">MASS DELETE</button>
                    </div>
                </div>

            </header>
            <hr>

            <div class="content">
                <form action="/" id="productlist_form" method="post">
                    <div class="row">

                        <?php
                        $id = 0;
                        $dvd = new Dvd();
                        $book = new Book();
                        $furniture = new Furniture();
                        $res = array($dvd->getProduct($id), $book->getProduct($id), $furniture->getProduct($id));

                        for ($row = 0; $row < count($res); $row++)
                        if ($res[$row]['response'] === '200' || '204') {
                            if (isset($res[$row]['dataArray'])) {
                                foreach ($res[$row]['dataArray'] as $result) { ?>
                                    <div class="column">
                                        <div class="card">
                                            <input type="checkbox" class="delete-checkbox" name="deletecheckbox[]" value="<?php echo $result['id'] ?>"><br><br>
                                            <?php
                                            echo $result['sku'] . '<br>';
                                            echo $result['name'] . '<br>';
                                            echo $result['price'] . ' $' . '<br>';
                                            if (isset($result['size'])) {
                                                echo  $result['size'] . ' MB' . '<br>';
                                            }
                                            if (isset($result['weight'])) {
                                                echo  $result['weight'] . ' KG'. '<br>';
                                            }
                                            if (isset($result['length'], $result['width'], $result['height'])) {
                                                echo  $result['length'] . 'x' . $result['width'] . 'x' . $result['height'] . ' CM' . '<br>';
                                            }
                                            ?>
                                        </div>
                                    </div><?php

                                }
                            }
                        }

                        ?>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <hr>
    <footer>
        <div class="footer">
            <p>Scandiweb Test assignment</p>
        </div>
    </footer>

</body>

</html>