<div class="row">
    <div class="col-25">
        <label for="Height">Height(CM)</label>
    </div>
    <div class="col-45">
        <input type="number" min="0" id="height" name="height" placeholder="#Height" required>
    </div>
    <div class="col-25">
        <label for="Width">Width(CM)</label>
    </div>
    <div class="col-45">
        <input type="number" min="0" id="width" name="width" placeholder="#Width" required>
    </div>
    <div class="col-25">
        <label for="Length">Length(CM)</label>
    </div>
    <div class="col-45">
        <input type="number" min="0" id="length" name="length" placeholder="#Length" required>
    </div>
</div>
<br>
<label for="attributesHeight">Please input height, width and length for furniture in CM</label>