<?php

namespace app\model;

class Furniture extends Product implements Product_Interface
{

    public function getProduct($id)
    {
        $query = "SELECT * FROM juniortest.products WHERE type = 'furniture' ";

        if ($id != 0) {
            $query .= "WHERE id = " . $id . " "; 
        }

        $query .= "ORDER BY id;";

        $res = $this->getResultSetArray($query);

        return $res;
       
    }

    public function setProduct()
    {
        if ($_POST['productType'] == 'furniture') {
            $sku = $_POST['sku'];
            $name = $_POST['name'];
            $price = $_POST['price'];
            $productType = $_POST['productType'];
            $length = $_POST['length'];
            $width = $_POST['width'];
            $height = $_POST['height'];

            $res = $this->connect();
            $query = "INSERT INTO juniortest.products (sku, name, price, type, length, width, height) VALUES ('$sku', '$name', '$price', '$productType', '$length', '$width', '$height')";

            $res = $this->getResultSetArray($query);

            return $res; 

        }
        
    }
    
}